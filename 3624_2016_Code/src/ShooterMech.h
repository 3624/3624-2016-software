#ifndef SRC_SHOOTERMECH_H_
#define SRC_SHOOTERMECH_H_

#include "WPIlib.h"


class ShooterMech : public AutomaticProcess
{
	public:
		ShooterMech();
		void Periodic();
		void CalcXYAngle();
		bool Rotate(double destinationAngle);
		void Buttons();
		void Init();

	private :
};
#endif /* SRC_SHOOTERMECH_H_ */
