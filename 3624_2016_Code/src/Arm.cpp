#include "Arm.h"
#include "Math.h"
#include "Robot.h"

extern CANTalon		*g_pElbow;
extern CANTalon		*g_pShoulder;

extern JS			*g_pARJS;
extern JS			*g_pALJS;
extern std::shared_ptr<NetworkTable> g_dataTable;
Arm::Arm()
{

}
void Arm::initADXL345I2C()
{
	i2cChannelElbow->Write(ADXL_CONFIG_PORT, 0);
	i2cChannelElbow->Write(ADXL_CONFIG_PORT, 16);
	i2cChannelElbow->Write(ADXL_CONFIG_PORT, 8);
	i2cChannelShoulder->Write(ADXL_CONFIG_PORT, 0);
	i2cChannelShoulder->Write(ADXL_CONFIG_PORT, 16);
	i2cChannelShoulder->Write(ADXL_CONFIG_PORT, 8);
}

void Arm::GetADXL345DataShoulder()
{
	short x_axis, y_axis, z_axis;
	unsigned char buff[TO_READ];

	i2cChannelShoulder->Read(ADXL_REG, TO_READ, buff);

	x_axis = (((int)buff[1]) << 8) | buff[0];
	y_axis = (((int)buff[3]) << 8) | buff[2];
	z_axis = (((int)buff[5]) << 8) | buff[4];

	//g_dataTable->PutNumber("X S", x_axis);
	//g_dataTable->PutNumber("Y S", y_axis);
	//g_dataTable->PutNumber("Z S", z_axis);

	m_shoulderZG = ((double)z_axis) * 16 / 4096;
	m_shoulderAng = acos(m_shoulderZG) * 180 / M_PI;
	//g_dataTable->PutNumber("Accel Angle S", m_shoulderAng);
	printf("SZG %.6f SA %.6f\n\r", m_shoulderZG, m_shoulderAng);
}

void Arm::GetADXL345DataElbow()
{
	short x_axis, y_axis, z_axis;
	unsigned char buff[TO_READ];

	i2cChannelElbow->Read(ADXL_REG, TO_READ, buff);

	x_axis = (((int)buff[1]) << 8) | buff[0];
	y_axis = (((int)buff[3]) << 8) | buff[2];
	z_axis = (((int)buff[5]) << 8) | buff[4];

	//g_dataTable->PutNumber("X E", x_axis);
	//g_dataTable->PutNumber("Y E", y_axis);
	//g_dataTable->PutNumber("Z E", z_axis);
	m_elbowZG = ((double)z_axis) * 16 / 4096;
	//g_dataTable->PutNumber("Elbow Z G", m_elbowZG);
	m_elbowAngRaw = acos(m_elbowZG) * 180 / M_PI;
	printf("EZG %.6f EARaw %.6f\n\r", m_elbowZG, m_elbowAngRaw);
	if(y_axis >= 0)
		m_elbowAngRaw += 90;
	else
	{
		m_elbowAngRaw = 90 - (m_elbowAngRaw);
	}
	m_elbowAngRel = m_elbowAngRaw - m_shoulderAng;
	//g_dataTable->PutNumber("Accel Angle E Rel", m_elbowAngRel);
}

void Arm::ConstructShoulder()
{
	/*double ps = g_dataTable->GetNumber("P-s", 1.2);
	double is = g_dataTable->GetNumber("I-s", 0.0005);
	double ds = g_dataTable->GetNumber("D-s", 0.0);*/
	g_pShoulder->SetControlMode(CANSpeedController::kSpeed);
	g_pShoulder->SetFeedbackDevice(CANTalon::QuadEncoder);
	g_pShoulder->SetPID(1.2, 0.0005, 0.0);//POSITION
	//g_pShoulder->SetPID(ps, is, ds);//SPEED
	g_pShoulder->ConfigEncoderCodesPerRev(7);
	g_pShoulder->SetSensorDirection(true);//NEEEEEEEEEEEEEED ATENTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!changed to true
	g_pShoulder->SetClosedLoopOutputDirection(false);//Check is this needs to be false***********
}

void Arm::ConstructElbow()
{
	/*double pe = g_dataTable->GetNumber("P-e", 1.2);
	double ie = g_dataTable->GetNumber("I-e", 0.001);
	double de = g_dataTable->GetNumber("D-e", 0.0);*/
	g_pElbow->SetControlMode(CANSpeedController::kSpeed);
	g_pElbow->SetFeedbackDevice(CANTalon::QuadEncoder);
	//g_pElbow->SetPID(pe, ie, de);
	g_pElbow->SetPID(1.2, 0.001, 0.0);
	g_pElbow->ConfigEncoderCodesPerRev(7);
	g_pElbow->SetSensorDirection(false);
	g_pElbow->SetClosedLoopOutputDirection(true);
}

void Arm::InitShoulder()
{
	g_dataTable->PutNumber("Max Speed Shoulder", 7500.0);

	g_dataTable->PutNumber("Shoulder Gain", 750.0);


	g_pShoulder->SetPosition(0);//zero shoulder to current position
}

void Arm::InitElbow()
{
	g_dataTable->PutNumber("Max Speed Elbow", 10000.0);


	g_dataTable->PutNumber("Elbow Gain", 1000.0);
	g_pElbow->SetPosition(0);//zero elbow to current position
}

double Arm::ScaleJoy(double joy)
{
	if(joy < -JOY_DEADBAND)
	{
		return (joy + JOY_DEADBAND)/(1- JOY_DEADBAND);
	}
	else if(joy > JOY_DEADBAND)
	{
		return (joy - JOY_DEADBAND)/(1- JOY_DEADBAND);
	}
	else
	{
		return 0;
	}
}

void Arm::ProcessShoulder()
{
	double maxSpeedShoulder = g_dataTable->GetNumber("Max Speed Shoulder", 7500.0);
	double gainShoulder     = g_dataTable->GetNumber("Shoulder Gain",      750.0);
	double shoulderPosition = g_pShoulder->GetPosition();
	double shoulderDestinationPosition = (ScaleJoy(g_pALJS->GetY() * -1) * 53);//was 53
	double shoulderError = fabs(shoulderPosition - shoulderDestinationPosition);

	//Speed = gain * error
	double shoulderSpeed = gainShoulder * shoulderError;

	if(shoulderSpeed > maxSpeedShoulder)
	{//Caps speed at maxSpeedShoulder
		shoulderSpeed = maxSpeedShoulder;
	}


	if(shoulderError < 0.5)
	{//Found destination position
		m_shoulderPos = 0;
	}
	else if(shoulderPosition < shoulderDestinationPosition)
	{
		m_shoulderPos = shoulderSpeed;
	}
	else
	{
		m_shoulderPos = -shoulderSpeed;
	}
}

void Arm::ProcessElbow()
{
	double elbowDestinationPosition = 0;

	double gainElbow = g_dataTable->GetNumber("Elbow Gain", 1000.0);
	double maxSpeedElbow = g_dataTable->GetNumber("Max Speed Elbow", 10000.0);
	double elbowPosition = g_pElbow->GetPosition();
	elbowDestinationPosition = (ScaleJoy(g_pARJS->GetY()) * 167);//was 250
	double elbowError = fabs(elbowPosition - elbowDestinationPosition);


	double elbowSpeed = gainElbow * elbowError;

	if(elbowSpeed > maxSpeedElbow)
	{//Caps speed at maxSpeedShoulder
		elbowSpeed = maxSpeedElbow;

	}

	//g_dataTable->PutNumber("Elbow Speed", elbowSpeed);

	if(elbowError < 0.5)
	{
		m_elbowPos = 0;
	}
	else if(elbowPosition < elbowDestinationPosition)
	{
		m_elbowPos = elbowSpeed;
	}
	else
	{
		m_elbowPos = -elbowSpeed;
	}
}

void Arm::ArmInit()
{

	i2cChannelElbow = new I2C(I2C::kOnboard, ADXL_ELBOW_SLAVE_ADDR);
	i2cChannelShoulder = new I2C(I2C::kOnboard, ADXL_SHOULDER_SLAVE_ADDR);

	m_shoulderPos = 0;
	m_elbowPos = 0;
	m_elbowMIN = 0;
	m_elbowMAX = 0;
	m_elbowAngRel = 0;
	m_elbowAngRaw = 0;
	m_shoulderAng = 0;
	m_elbowZG = 0;
	m_shoulderZG = 0;
	m_shoulderMIN = 0;
	m_lawofCosines = 0;
	m_bZeroElbow = true;
	m_bZeroShoulder = true;
	m_elbowDisplacement = 9.5;
	ConstructShoulder();
	ConstructElbow();
	InitShoulder();
	InitElbow();
	initADXL345I2C();
	m_currentElbow = g_pElbow->GetPosition();
}

void Arm::CheckArmLimits()
{
	double e1 = m_shoulderAng;//fmod((g_pShoulder->GetPosition() * 5.2941), 360);//Shoulder angle
	double e2 = ((m_elbowPos - m_elbowDisplacement) * ELBOW_ANG_RANGE / ELBOW_ENC_RANGE) + ELBOW_DOWN;//m_elbowAngRel;//fmod((g_pElbow->GetPosition()    * 1.35995) /*- 125*/, 360);
	double a = 16/*was 18*/ * sin(e1 * M_PI/180) + 15/*was 15.5*/ * sin((e2 - e1)*M_PI/180);
	//g_dataTable->PutNumber("A", a);
	m_elbowMIN = (asin((15 - 16 * sin(e1 * M_PI/180))/15)*180/M_PI + e1);
	m_elbowMAX = (acos((15 - 16 * sin(e1 * M_PI/180))/15)*180/M_PI + 90 + e1);
	//g_dataTable->PutNumber("Elbow MAX", m_elbowMAX);
	//g_dataTable->PutNumber("Elbow MIN", m_elbowMIN);
	//g_dataTable->PutNumber("E1", e1);
	//g_dataTable->PutNumber("E2", e2);
	//g_dataTable->PutNumber("Current Elbow", m_currentElbow);
	if(m_currentElbow < m_elbowMIN && (m_elbowMIN < e2))
	{//allows for movement to farthest possible location given that desired pos is past min angle of range
		m_elbowPos = m_elbowMIN / 1.35995;
		//g_dataTable->PutNumber("Undefined", -1);
	}
	else if(m_currentElbow > m_elbowMAX && (e2 < m_elbowMAX))
	{//allows for movement to farthest possible location given that desired pos is pas max angle of range
		m_elbowPos = m_elbowMAX / 1.35995;
		//g_dataTable->PutNumber("Undefined", 1);
	}
	else if(a < 15)
	{//allows for movement to desired location
		//g_dataTable->PutNumber("Undefined", 0);
	}
	m_currentElbow = ((g_pElbow->GetPosition() - m_elbowDisplacement) * ELBOW_ANG_RANGE / ELBOW_ENC_RANGE) + ELBOW_DOWN;//g_pElbow->GetPosition() * 1.35995 /*- 125*/;

	/*
	//////////////////////////////////////D Code I AM D CAPITAN NAO
	double e3 = m_shoulderPos * 5.2941;
	double e4 = m_elbowAngRel;
	m_lawofCosines = sqrt(pow(16, 2) + pow(15, 2) - 2 * 16 * 15 * cos(e4 * M_PI / 180));

	if(e4 < 180)
	{
		m_shoulderMIN = (90-((asin(15 * (sin (e3 * M_PI / 180)))/(m_lawofCosines))* 180 / M_PI)-((acos(15/(m_lawofCosines)))* 180 / M_PI));
	}
	else
	{
		m_shoulderMIN = (((asin(15/(m_lawofCosines)))* 180 / M_PI)+((asin((15 * ((sin(360-e4)) * 180 / M_PI))/(m_lawofCosines)))* 180 / M_PI));
	}
	if(m_shoulderPos * 5.2941 <= m_shoulderMIN)
	{
		m_shoulderPos = m_shoulderMIN / 5.2941;
	}

	*/
	//////////////////////////////////////END OF D CODE
}

void Arm::ZeroElbow()
{
	if(m_bZeroElbow)
		{
			if(abs(ELBOW_DOWN - m_elbowAngRel) < 2)
			{
				m_elbowDisplacement = m_elbowAngRel;
				m_bZeroElbow = false;
				m_elbowPos = ELBOW_DOWN + m_elbowDisplacement;
				m_currentElbow = ((g_pElbow->GetPosition() - m_elbowDisplacement) * ELBOW_ANG_RANGE / ELBOW_ENC_RANGE) + ELBOW_DOWN;
			}
			else
			{
				m_elbowPos += .005;
			}
		}
}

void Arm::ArmPeriodic()
{
		GetADXL345DataShoulder();
		GetADXL345DataElbow();
		ProcessShoulder();
		ProcessElbow();
		//CheckArmLimits();
		g_dataTable->PutNumber("Elbow Pos", g_pElbow->GetPosition());
		g_dataTable->PutNumber("Shoulder Pos", g_pShoulder->GetPosition());
		g_pElbow->Set(m_elbowPos);
		g_pShoulder->Set(m_shoulderPos);
		g_dataTable->PutNumber("Shoulder Error", g_pShoulder->GetPosition() - m_shoulderPos);
		g_dataTable->PutNumber("Elbow Error", g_pElbow->GetPosition() - m_elbowPos);
		//printf("P:%.6f I:%.6f D:%.6f\n\r", g_pShoulder->GetP(), g_pShoulder->GetI(), g_pShoulder->GetD());

}
