#ifndef SRC_AUTONOMOUS_H_
#define SRC_AUTONOMOUS_H_

#include "AutomaticProcess.h"
#include "WPILib.h"
#include "RunMode.h"

#define HROBOT 11
#define HTARGET 92
#define DELTA_HEIGHT HTARGET-HROBOT

typedef enum
{
	eAutoStart,
	eDriveObs,
	ePanCamera,
	eCalcAngle,
	eRotate,
	eCalcXY,
	eDriveLin,
	eAim,
	eFire,
	eEnd
}eAutonomousState;


class CAutonomous : public AutomaticProcess
{
	public    : CAutonomous();
				virtual ~CAutonomous();

				void Init();
				void Periodic();
				void PeriodicOld();
				void autoStart();
				void driveObs();
				void panCamera();
				void calcAngle();
				void rotate();
				void calcXY();
				void driveLin();
				void aim();
				void fire();
				void search();

				AutomaticOpList m_list;

				float m_speedLeft;
				float m_speedRight;

				float m_distanceRot;
				float m_distance;
				float m_destX;
				float m_destY;

				float m_start;
				float m_speed;
				float m_time;
				float m_distanceLin;
				float m_deltaTime;
				float m_destinationAngle;
				float m_deltaH;
				double m_camXError;
				double m_camYError;
				double m_move;
				double m_UltrasonicDistance;

				double m_hypotenuse;
				double m_theta;
				double m_distanceToSS;


};
#endif
