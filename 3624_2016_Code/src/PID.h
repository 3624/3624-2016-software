/*
 * PID.h
 *
 *  Created on: Feb 5, 2016
 *      Author: NYSC
 */

#ifndef SRC_PID_H_
#define SRC_PID_H_

#include <WPILib.h>

class PID
{
	public:		PID();
				virtual ~PID();

				float linear(float encoderLeft, float encoderRight, float setPoint);
				float rotational(float encoderVal, float setPoint);

				void periodic();//update
};

#endif /* SRC_PID_H_ */
