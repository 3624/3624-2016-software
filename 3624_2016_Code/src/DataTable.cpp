/*
 * DataTable.cpp
 *
 *  Created on: Feb 7, 2016
 *      Author: Michael
 */
#include "Robot.h"

extern RobotDrive *g_pDrive;
extern Joystick   *g_pRJS;
extern Joystick   *g_pLJS;
extern CPanTilt   *g_pPanTilt;
extern CANTalon	*g_pmotor1;
extern CANTalon    *g_pElbow;
extern CANTalon   *g_pShoulder;
extern AHRS       *g_pAHRS;
extern CANTalon   *g_pleftRear;
extern CANTalon   *g_prightRear;
extern ADXL345_I2C *g_pAccel;
extern DigitalInput *g_pShoulderLimit1;
extern DigitalInput *g_pShoulderLimit2;
extern DigitalInput *g_pElbowLimit1;
extern DigitalInput *g_pElbowLimit2;
extern Arm 		 *g_pArm;


extern std::shared_ptr<NetworkTable> g_dataTable;
extern std::shared_ptr<NetworkTable> g_dTA;

void Robot::InitDataTable()
{
	g_dataTable = NetworkTable::GetTable("Popcorn");
	g_dTA = NetworkTable::GetTable("Auto Data Table");
}

void Robot::UpdateDataTable()
{

}
