/*
 * AutonomousList.h
 *
 *  Created on: Feb 10, 2016
 *      Author: NYSC
 */

#ifndef AUTONOMOUSLIST_H_
#define AUTONOMOUSLIST_H_

#include <vector>

#include "AutonomousOperation.h"

typedef std::vector<CAutonomousOperation *> AutonomousOps;

class CAutonomousList
{
	public:
		void Add(CAutonomousOperation *pOP)
		{
			m_ops.push_back(pOP);
		}

		//returns true when done, false otherwise
		bool Periodic()
		{
			if (m_ops.size() == 0)
				return true;

			AutonomousOps::iterator iter = m_ops.begin();

			if ((*iter)->Periodic())
			{
				delete (*iter);

				m_ops.erase(iter);
			}

			return false;
		}

		void Clear()
		{
			for(AutonomousOps::iterator iter = m_ops.begin(); iter!=m_ops.end();iter++)//evan dopes on adderall
			{
				delete (*iter);
			}
			m_ops.clear();
		}

	protected:
		AutonomousOps m_ops;


};


#endif /* AUTONOMOUSLIST_H_ */
