#include "Autonomous.h"
#include "navx_frc_cpp\src\AHRS.h"
#include "PanTilt.h"
#include "Math.h"
#include <PID.h>

CAutonomous::CAutonomous()
{

}


CAutonomous::~CAutonomous()
{

}

void CAutonomous::Init()
{
	AutomaticProcess::Init();
}

void CAutonomous::Periodic()
{
	AutomaticProcess::Periodic();

}
