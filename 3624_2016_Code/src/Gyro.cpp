/*
 * Gyro.cpp
 *
 *  Created on: Feb 7, 2016
 *      Author: Michael
 */
#include "Robot.h"

extern AHRS       *g_pAHRS;

void Robot::InitGyro()
{
	try
	    {
	    	m_pAHRS = new AHRS(SPI::Port::kMXP);
	    }
	    catch (std::exception &ex)
	    {
	    	std::string err_string = "Error instantiating navX MXP:  ";
	        err_string += ex.what();
	        DriverStation::ReportError(err_string.c_str());
	    }
	    if (m_pAHRS)
	    {

	    }
	    g_pAHRS = m_pAHRS;
}
