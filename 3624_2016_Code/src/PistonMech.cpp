/*
 * PistonMech.cpp
 *
 *  Created on: Feb 29, 2016
 *      Author: Michael
 */
#include "PistonMech.h"
#include "Robot.h"

extern DoubleSolenoid *g_pRightBig;
extern DoubleSolenoid *g_pLeftBig;
extern DoubleSolenoid *g_pRightSmall;
extern DoubleSolenoid *g_pLeftSmall;
extern JS			  *g_pARJS;
extern PistonMech	  *g_pPiston;

PistonMech::PistonMech()
{
	bigPistonToggle = false;
	bigPistonState = compress;
	smallPistonState  = compress;
}

//extends small piston
void PistonMech::ExtendSmall()
{
	smallPistonState = extend;
}

//extends large piston
void PistonMech::ExtendBig()
{
	bigPistonState = extend;
}

//closes big piston
void PistonMech::CompressBig()
{
	bigPistonState = compress;
}

//runs piston functions
void PistonMech::Update()
{
	if(g_pARJS->IsHeld(7) && g_pARJS->IsPressed(3))
	{
		ExtendSmall();
	}
	if(g_pARJS->IsHeld(7) && g_pARJS->IsPressed(5))
	{
		bigPistonToggle = !bigPistonToggle;
		if(bigPistonToggle)
			ExtendBig();
		else if(!bigPistonToggle)
			CompressBig();
	}

	g_pRightSmall->Set(smallPistonState);
	g_pLeftSmall->Set(smallPistonState);
	g_pRightBig->Set(bigPistonState);
	g_pLeftBig->Set(bigPistonState);
}

//runs pistons
void Robot::UpdatePiston()
{
	g_pPiston->Update();
}

