#ifndef SRC_AUTONOMOUSOPERATION_H_
#define SRC_AUTONOMOUSOPERATION_H_

//#include <WPILib.h>

//#include "Autonomous.h"

class CAutonomousOperation
{
	public:
		CAutonomousOperation();
		virtual ~CAutonomousOperation();

		virtual bool Process();
		virtual void Init();
		virtual bool Periodic();

		bool m_bInitialized;
		bool m_bIsPositive;
};

class CLinearMovementOp : public CAutonomousOperation
{
	public:
		CLinearMovementOp(double speed, double distance, double P, double I, double D);
		~CLinearMovementOp();

		bool Process();
		void Init();

	private:
		double m_speed, m_distance;
};

class CRotationOp : public CAutonomousOperation
{
	public:
		CRotationOp(double destinationAngle, bool bZeroYaw = false);
		CRotationOp(bool checkX);
		~CRotationOp();

		bool Process();
		void Init();

		double m_destinationAngle;
		bool   m_bZeroYaw;
		bool   m_bCheckX;
};

class CAcquireTowerOp : public CAutonomousOperation
{
	public:
		CAcquireTowerOp();
		~CAcquireTowerOp();

		bool Process();
		void Init();
		bool Search();

		CRotationOp m_rotate;

		bool m_bHasTower;
		double m_move;
};

#endif
