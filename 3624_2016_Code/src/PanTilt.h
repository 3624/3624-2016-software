#ifndef SRC_PANTILT_H_
#define SRC_PANTILT_H_

#include <WPILib.h>

#define PAN_0_DEGREES  0.500
#define TILT_0_DEGREES 0.644

class CPanTilt
{
	public    : CPanTilt();

				void SetServos(Servo *pPan, Servo *pTilt);

				void SetPanTilt(double pan, double tilt);
				void SetPan(double pan);
				void SetTilt(double tilt);
				double GetPan();
				double GetTilt();

				double GetPanAngle();
				double GetTiltAngle();
				double GetPanAngleRad();
				double GetTiltAngleRad();
				void UpdatePanTiltAngle();

	protected :
				Servo *m_pPan, *m_pTilt;

				double m_pan, m_tilt;

				double m_panAngle, m_tiltAngle;
};

#endif
