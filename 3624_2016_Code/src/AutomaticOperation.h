#ifndef SRC_AUTOMATICOPERATION_H_
#define SRC_AUTOMATICOPERATION_H_

//#include <WPILib.h>

#include "AutomaticProcess.h"

class AutomaticOperation
{
	public:
	AutomaticOperation();
		virtual ~AutomaticOperation();

		virtual bool Process();
		virtual void Init();
		virtual bool Periodic();

		bool m_bInitialized;
		bool m_bIsPositive;
};

class CLinearMovementOp : public AutomaticOperation
{
	public:
		CLinearMovementOp(double speed, double distance, double P, double I, double D);
		~CLinearMovementOp();

		bool Process();
		void Init();

	private:
		double m_speed, m_distance;
};

class CRotationOp : public AutomaticOperation
{
	public:
		CRotationOp(double destinationAngle, bool bZeroYaw = false);
		CRotationOp(bool checkX);
		~CRotationOp();

		bool Process();
		void Init();

		double m_destinationAngle;
		bool   m_bZeroYaw;
		bool   m_bCheckX;
};

class CAcquireTowerOp : public AutomaticOperation
{
	public:
		CAcquireTowerOp();
		~CAcquireTowerOp();

		bool Process();
		void Init();
		bool Search();

		CRotationOp m_rotate;

		bool m_bHasTower;
		double m_move;
};

#endif
