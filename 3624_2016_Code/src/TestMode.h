/*
 * TestMode.h

 *
 *  Created on: Jan 27, 2016
 *      Author: Michael
 */
#include "RunMode.h"

#ifndef SRC_TESTMODE_H_
#define SRC_TESTMODE_H_

class CTestMode : public CRunMode
{
	public: CTestMode();
			virtual ~CTestMode();
			virtual void Periodic();
			virtual void Init();

			LiveWindow *m_pLiveWindow;
};

#endif /* SRC_TESTMODE_H_ */
