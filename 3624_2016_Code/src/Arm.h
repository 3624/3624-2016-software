#include "Math.h"
#include "WPILib.h"

#ifndef SRC_POPCORN7_H_
#define SRC_POPCORN7_H_

#define ADXL_SHOULDER_SLAVE_ADDR 0x53
#define ADXL_ELBOW_SLAVE_ADDR 0x1D
#define ADXL_CONFIG_PORT 0x2D
#define ADXL_REG 0x32
#define TO_READ 6
#define ELBOW_DOWN 15
#define JOY_DEADBAND .05
#define ELBOW_ENC_RANGE 337
#define ELBOW_ANG_RANGE 350

class Arm
{
	public    : Arm();
				void ConstructShoulder();
				void ConstructElbow();
				void ProcessShoulder();
				void ProcessElbow();
				void CheckArmLimits();
				void initADXL345I2C();
				void GetADXL345DataElbow();
				void GetADXL345DataShoulder();
				void InitShoulder();
				void InitElbow();
				void ArmInit();
				void ArmPeriodic();
				void ZeroElbow();
				double ScaleJoy(double joy);


				//ADXL345_I2C m_accelElbow;
				//ADXL345_I2C m_accelShoulder;
				I2C *i2cChannelShoulder;
				I2C *i2cChannelElbow;
				double m_elbowMIN;
				double m_elbowMAX;
				double m_currentElbow;
				double m_elbowPos;
				double m_shoulderPos;
				double m_elbowAngRaw;
				double m_elbowAngRel;
				double m_shoulderAng;
				double m_elbowZG;
				double m_shoulderZG;
				double m_shoulderMIN;
				double m_lawofCosines;
				bool   m_bZeroElbow;
				bool   m_bZeroShoulder;
				double m_elbowDisplacement;

};

#endif
