#include "AutonomousOperation.h"
#include "navx_frc_cpp\src\AHRS.h"
#include "PanTilt.h"
#include "AutonomousList.h"
#include "WPIlib.h"


extern AHRS     *g_pAHRS;
extern CANTalon *g_pLeftRear, *g_pRightRear;
extern RobotDrive *g_pDrive;
extern CPanTilt *g_pPanTilt;
extern CAutonomousList *g_pAutoList;
extern float g_currentX;

extern std::shared_ptr<NetworkTable> g_dataTable;

void GetTarget(int &errorX, int &errorY);

CAutonomousOperation::CAutonomousOperation()
{
	m_bInitialized = false;
}

CAutonomousOperation::~CAutonomousOperation()
{
}

void CAutonomousOperation::Init()
{
	m_bInitialized = true;
}

bool CAutonomousOperation::Periodic()
{
	if (!m_bInitialized)
	{
		Init();

		if (!m_bInitialized)
		{
			return false;
		}
	}

	return Process();
}

bool CAutonomousOperation::Process()
{
	return true;
}

CLinearMovementOp::CLinearMovementOp(double speed, double distance, double p, double i, double d)
{
	m_speed    = speed;
	m_distance = distance;

	if (distance < 0)
	{
		m_speed = -m_speed;
	}

	//g_pLeftRear->SetPID(p, i, d);
	//g_pRightRear->SetPID(p, i, d);
}

CLinearMovementOp::~CLinearMovementOp()
{
}

void CLinearMovementOp::Init()
{
	if (g_pLeftRear->GetPosition() == 0 && g_pRightRear->GetPosition() == 0)
	{
		m_bInitialized = true;
	}
	else
	{
		g_pLeftRear->SetPosition(0);
		g_pRightRear->SetPosition(0);
	}
}

bool CLinearMovementOp::Process()
{
	double leftRearPosition  = g_pLeftRear->GetPosition();
	double rightRearPosition = g_pRightRear->GetPosition();

	g_dataTable->PutNumber("Left Encoder",  leftRearPosition);
	g_dataTable->PutNumber("Right Encoder", rightRearPosition);

	if(abs(rightRearPosition) <= abs(m_distance))
	{
		g_pDrive->ArcadeDrive(m_speed,0);
	}
	else
	{
		g_pDrive->ArcadeDrive(0.0, 0.0);

		return true;
	}

	g_dataTable->PutNumber("Left Error", g_pLeftRear->GetClosedLoopError());
	g_dataTable->PutNumber("Right Error", g_pRightRear->GetClosedLoopError());

	return false;
}

CRotationOp::CRotationOp(double destinationAngle, bool bZeroYaw)
{
	m_destinationAngle = destinationAngle;
	m_bZeroYaw         = bZeroYaw;
	m_bCheckX 		   = false;
}
CRotationOp::CRotationOp(bool checkX)
{
	m_destinationAngle = 0;
	m_bZeroYaw = false;
	m_bCheckX = checkX;
}

CRotationOp::~CRotationOp()
{
}

void CRotationOp::Init()
{
	if(!m_bZeroYaw)//false
	{
		m_bInitialized = true;
	}
	else//true
	{
		g_pAHRS->ZeroYaw();

		if(g_pAHRS->GetYaw() == 0)
		{
			m_bInitialized = true;
		}
	}
}

bool CRotationOp::Process()
{
	//double leftRearPosition = g_pLeftRear->GetPosition();
	//double rightRearPosition = g_pRightRear->GetPosition();
	//g_dataTable->PutNumber("Left Encoder", leftRearPosition);
	//g_dataTable->PutNumber("Right Encoder", rightRearPosition);

	double deltaAngle;
	if(m_bCheckX)
	{
		if(g_currentX >= 0)//not sure if this is right
		{
			deltaAngle = 90 - g_pAHRS->GetYaw();
		}
		else if(g_currentX < 0)
		{
			deltaAngle = 270 + g_pAHRS->GetYaw();
		}
	}
	else
	{
		deltaAngle = g_pAHRS->GetAngle() - m_destinationAngle;
	}
    double rotateValue;

	//g_dataTable->PutNumber("DELTA ANGLE ********", deltaAngle);

	if (deltaAngle > 180.0)
	{
		deltaAngle = 360-deltaAngle;
		deltaAngle *= -1;
	}
	else if(deltaAngle < -180)
	{
		deltaAngle += 180;
		deltaAngle *= -1;
	}

	if(fabs(deltaAngle) < 0.5)
	{
		g_pDrive->ArcadeDrive(0.0, 0.0);
		return true;
	}

	double speed = -((0.80 / 180.0) * fabs(deltaAngle) + 0.20);
	rotateValue = speed;

	if(deltaAngle > 0)
	{
		rotateValue = -speed;
	}
	printf("deltaAngle: %.6f, CurrentAngle: %.6f, Speed: %.6f",deltaAngle, g_pAHRS->GetAngle(), rotateValue);

	g_pDrive->ArcadeDrive(0.0, rotateValue, false);

	return false;
}

CAcquireTowerOp::CAcquireTowerOp()
:m_rotate(0,false)
{
	m_bHasTower = false;
}

CAcquireTowerOp::~CAcquireTowerOp()
{
}

void CAcquireTowerOp::Init()
{
	m_bInitialized = true;
}

bool CAcquireTowerOp::Process()
{
	int errorX, errorY;

	GetTarget(errorX,errorY);

	if(errorX == -1000 || errorY == -1000)
	{
		m_bHasTower = false;

		Search();

		m_rotate.Init();
		m_rotate.m_destinationAngle = 0;
		return m_rotate.Process();
	}
	else
	{
		m_bHasTower = true;
		return true;
	}
	return false;
}
bool CAcquireTowerOp::Search()
{
	double newPan  = g_pPanTilt->GetPan();
	double newTilt = .7;
	if(newPan >= .97 || newPan <= .03)
	{
		m_move *= -1;
		return true;
	}
	newPan += m_move;
	g_pPanTilt->SetPanTilt(newPan, newTilt);
	g_dataTable->PutNumber("newTilt", newTilt);
	g_dataTable->PutNumber("newPan", newPan);
	return false;
}
