/*
 * Joystick.h
 *
 *  Created on: Feb 13, 2016
 *      Author: Michael
 */

#ifndef SRC_JS_H_
#define SRC_JS_H_
#include "WPIlib.h"

class JS : public Joystick
{
	public :
		JS(int i);
		void UpdateJoystick();
		bool IsHeld(int i);
		bool IsPressed(int i);
		bool IsReleased(int i);
		bool m_bButtons [12], m_bButtonUp[12], m_bButtonDown[12];
		int  m_buttonCounter[12];
};

#endif /* SRC_JS_H_ */

