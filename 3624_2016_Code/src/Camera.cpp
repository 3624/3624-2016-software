/*
 * Camera.cpp

 *
 *  Created on: Feb 7, 2016
 *      Author: Michael Wolf-Sonkin
 */

#include "Robot.h"
#include "Math.h"


int g_errorX = -1000;
int g_errorY = -1000;
double m_move = 0.004;
extern std::shared_ptr<NetworkTable> g_dataTable;
extern std::mutex g_targetMutux;
extern CPanTilt   *g_pPanTilt;
extern AHRS       *g_pAHRS;

void GetTarget(int &errorX, int &errorY)
{
	g_targetMutux.lock();

	errorX = g_errorX;
	errorY = g_errorY;

	g_targetMutux.unlock();
}

void SetTarget(int errorX, int errorY)
{
	g_targetMutux.lock();

	g_errorX = errorX;
	g_errorY = errorY;

	//printf("%d,%d\n", g_errorX, g_errorY);

	g_targetMutux.unlock();
}

void SerialThread()
{
	SerialPort serial(115200);

	serial.EnableTermination('\n');

	serial.SetReadBufferSize(1024);

	do
	{
		char readData[100];

		int bytesRead = serial.Read(readData, 100);

		if (bytesRead > 0)
		{
			readData[bytesRead] = 0;

			char *pSep = strstr(readData, ",");

			if (pSep)
			{
				*pSep = 0;

				for (size_t index=0; index<strlen(pSep); index++)
				{
					if (pSep[index] == '\n')
					{
						pSep[index] = 0;
						break;
					}
				}

				int errorX, errorY;

				sscanf(readData, "%d", &errorX);
				sscanf(&pSep[1], "%d", &errorY);

				SetTarget(errorX, errorY);
			}
		}

	} while (1);
}

void Search()
{
	double newPan  = g_pPanTilt->GetPan();
	double newTilt = .7;
	if(newPan >= .97 || newPan <= .03)
	{
		m_move *= -1;
	}
	newPan += m_move;

	//g_pPanTilt->SetPanTilt(newPan,newTilt);
	g_pPanTilt->SetPan(newPan);

	//g_dataTable->PutNumber("Tilt Value", newTilt);
	//g_dataTable->PutNumber("Pan Value", newPan);
}

void TrackTarget()
{
	int camXError, camYError;

	GetTarget(camXError, camYError);

	g_dataTable->PutNumber("CAM_ERROR_X", camXError);
	g_dataTable->PutNumber("CAM_ERROR_Y", camYError);

	double newPan;
	double newTilt;

	if (camXError == -1000 || camYError == -1000)
	{
		Search();
	}
	else
	{
		newPan  = g_pPanTilt->GetPan()  + .00001f * camXError;
		newTilt = g_pPanTilt->GetTilt() - .00001f * camYError;

		g_pPanTilt->SetPanTilt(newPan, newTilt);
	}

}

