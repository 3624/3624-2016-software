#include "WPILib.h"

#ifndef PISTON_MECH_H
#define PISTON_MECH_H

class PistonMech
{
	public:
		PistonMech();
		void ExtendSmall();
		void ExtendBig();
		void CompressBig();

		void Update();
		bool bigPistonToggle;

		enum DoubleSolenoid::Value extend = DoubleSolenoid::kForward;
		enum DoubleSolenoid::Value compress = DoubleSolenoid::kReverse;

		enum DoubleSolenoid::Value bigPistonState;
		enum DoubleSolenoid::Value smallPistonState;
};

#endif
