#include "ShooterMech.h"
#include "JS.h"
#include "Robot.h"
#include "Math.h"
#include "PanTilt.h"

extern CANTalon 	*g_pFan, *g_pBarrel, *g_pShield, *g_pShootWheels;
extern Servo		*g_pFireServo;
extern JS			*g_pARJS;
extern JS			*g_pALJS;

extern JS			*g_pDRJS;
extern JS			*g_pDLJS;

extern ShooterMech 	*g_pShoot;
extern float		g_currentX;
extern float		g_currentY;
extern CANTalon *g_pLeftRear, *g_pRightRear, *g_pLeftFront, *g_pRightFront;
extern RobotDrive *g_pDrive;
extern AHRS			*g_pAHRS;
extern DigitalInput	*g_pBarrelLimit1;
extern DigitalInput	*g_pShieldLimit1;
extern DigitalInput *g_pBallSensor;
extern std::shared_ptr<NetworkTable> g_dataTable;
extern CPanTilt      *g_pPanTilt;

ShooterMech::ShooterMech()
{
}

void ShooterMech::Init()
{
	AutomaticProcess::Init();

	m_eShootState = eIdle;
	m_fanSpeed = 0;
	m_servoPosition = SERVO_CLOSED;
	m_wheelSpeed = 0;
	m_barrelPosition = g_pBarrel->GetPosition();
	m_shieldPosition = g_pShield->GetPosition();
	m_bCalcedAngle = false;
	m_macroAngle = 0;
	m_bWheel = false;
	m_bZeroBarrel = true;
	m_bZeroShield = true;
	m_barrelDisplacement = 0;
	m_shieldDisplacement = 0;
	m_counter = 0;
	m_bisRotatingToCam = false;
}

void Robot::InitShoot()
{
	g_pShoot->Init();
}

//assigns functions
void ShooterMech::Buttons()
{
	if(g_pALJS->IsHeld(7) && m_eShootState == eIdle)
	{
		if(g_pALJS->IsPressed(1))//release servo to fire
		{
			if(m_servoPosition == SERVO_CLOSED)
			{
				m_servoPosition = SERVO_OPEN;
			}
			else
			{
				m_servoPosition = SERVO_CLOSED;
			}
		}
		if(g_pALJS->IsPressed(2))//Toggle Shooting Wheel
		{
			//printf("Pressed\n\r");
			if(!m_bWheel)
			{
				m_wheelSpeed = SHOOT_SPEED;
				//printf("Speed is -4000\n\r");
			}
			else
			{
				m_wheelSpeed = 0;
				//printf("Speed is 0\n\r");
			}
			m_bWheel = !m_bWheel;
		}
		if(g_pALJS->IsHeld(9))//Shield +
		{
			m_shieldPosition+=.01;
		}
		if(g_pALJS->IsHeld(10))//Shield -
		{
			m_shieldPosition-=.01;
		}
		if(g_pALJS->IsHeld(11))//PickUp +
		{
			m_barrelPosition+=.01;
		}
		if(g_pALJS->IsHeld(12))//PickUp -
		{
			m_barrelPosition-=.01;
		}
		Fan();
	}
	else
	{
		if(g_pALJS->IsPressed(1))//go to firing state for macro if it is initiated
		{
			m_eShootState = ePickup;
		}
		if(g_pALJS->IsPressed(4))//start macro by aiming
		{
			m_eShootState = eLowAim;
		}
		if(g_pALJS->IsPressed(6))//make robot ready for picking up ball
		{
			m_eShootState = eAiming;
		}
	}
	if(g_pALJS->IsPressed(8))//Reset
	{
		m_eShootState = eReset;
	}

	int errorX, errorY;
	extern void GetTarget(int &errorX, int &errorY);
	GetTarget(errorX, errorY);

	if(g_pARJS->IsPressed(1) && !m_bisRotatingToCam && errorX != -1000 && errorY != -1000)
	{
		m_bisRotatingToCam = true;

		m_goToPanAngle = g_pAHRS->GetAngle() + g_pPanTilt->GetPanAngle();

		if(m_goToPanAngle >= 360)
		{
			m_goToPanAngle = fmod(m_goToPanAngle, 360);
		}
		else if(m_goToPanAngle < 0)
		{
			m_goToPanAngle = 360 + m_goToPanAngle;
		}

	}
	if(m_bisRotatingToCam)
	{
		m_bisRotatingToCam = !Rotate(m_goToPanAngle);
	}
	else
	{
		g_pDrive->ArcadeDrive(-g_pDLJS->GetY(), -g_pDLJS->GetZ());
	}
	printf("PanAngle: %.6f\r\n", g_pPanTilt->GetPanAngle());
}

void ShooterMech::Periodic()
{
	if(!m_bZeroBarrel && !m_bZeroShield)
	{
		switch(m_eShootState)
		{
			case eIdle :
				Idle();
				//printf(" Idle");
				break;
			case ePickup :
				Pickup();
				//printf(" Pickup");
				break;
			case eAiming :
				Aim();
				//printf(" Aim");
				break;
			case eFiring :
				Fire();
				//printf(" Fire");
				break;
			case eWait :
				//printf(" Wait");
				Wait();
				break;
			case eLowAim :
				//printf(" Low Aim");
				LowAim();
				break;
			case eLowFire :
				//printf(" Low Fire");
				LowFire();
				break;
			case eReset :
				//printf(" Reset");
				Reset();
				break;
		}
	}
	Buttons();
	CheckLimits();
	ZeroBarrel();
	ZeroShield();
	g_pFan->Set(m_fanSpeed);
	g_pBarrel->SetSetpoint(m_barrelPosition);\

	//printf("\r\n%.6fShield Pos: ",m_shieldPosition);

	g_pShield->SetSetpoint(m_shieldPosition);

	//printf("\r\nCommanded Wheel Speed: %.6f", m_wheelSpeed);

	g_pShootWheels->Set(m_wheelSpeed);
	g_pFireServo->Set(m_servoPosition);
}

void Robot::UpdateShoot()
{
	g_pShoot->Periodic();
}
