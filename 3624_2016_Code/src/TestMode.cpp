/*
 * TestMode.cpp
 *
 *  Created on: Jan 27, 2016
 *      Author: Michael
 */

#include <TestMode.h>

CTestMode::CTestMode()
{
}

CTestMode::~CTestMode()
{
}

void CTestMode::Init()
{
	m_pLiveWindow = LiveWindow::GetInstance();
}

void CTestMode::Periodic()
{

	m_pLiveWindow->Run();
}

