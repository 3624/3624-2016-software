#include "PanTilt.h"
#include "Math.h"

CPanTilt::CPanTilt()
{
	m_pan   = PAN_0_DEGREES;
	m_tilt  = TILT_0_DEGREES;
	m_pPan  = NULL;
	m_pTilt = NULL;
	UpdatePanTiltAngle();
}

void CPanTilt::SetServos(Servo *pPan, Servo *pTilt)
{
	m_pPan = pPan;
	m_pTilt = pTilt;
}

void CPanTilt::SetPanTilt(double pan, double tilt)
{
	if (m_pPan && m_pTilt)
	{
		m_pPan->Set(pan);
		m_pTilt->Set(tilt);

			m_pan = pan;
		m_tilt = tilt;
	}
}
void CPanTilt::SetPan(double pan)
{
	if (m_pPan)
	{
		m_pPan->Set(pan);

		m_pan = pan;
	}
}

void CPanTilt::SetTilt(double tilt)
{
	if (m_pTilt)
	{
		m_pTilt->Set(tilt);

		m_tilt = tilt;
	}
}

double CPanTilt::GetPan()
{
	return m_pan;
}

double CPanTilt::GetTilt()
{
	return m_tilt;
}

double CPanTilt::GetTiltAngle()
{
	UpdatePanTiltAngle();
	return m_tiltAngle;
}
double CPanTilt::GetPanAngle()
{
	UpdatePanTiltAngle();
	return m_panAngle;
}

double CPanTilt::GetTiltAngleRad()
{
	return m_tiltAngle*M_PI/180;
}
double CPanTilt::GetPanAngleRad()
{
	return m_panAngle*M_PI/180;
}

void CPanTilt::UpdatePanTiltAngle()
{
	m_panAngle  = ((GetPan()-.5)*180);
	m_tiltAngle = ((GetTilt()-.6)*185) + 8.955;
}

