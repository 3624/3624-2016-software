#include "WPILib.h"
#include "Robot.h"

RobotDrive *g_pDrive   = NULL;

JS		   *g_pDRJS     = NULL;
JS		   *g_pDLJS     = NULL;
JS		   *g_pALJS	   = NULL;
JS		   *g_pARJS	   = NULL;

CPanTilt   *g_pPanTilt = NULL;
CANTalon	*g_pmotor1 = NULL;
CANTalon    *g_pElbow = NULL;
CANTalon   *g_pShoulder = NULL;
AHRS       *g_pAHRS    = NULL;

CANTalon   *g_pLeftRear = NULL;
CANTalon   *g_pRightRear = NULL;
CANTalon   *g_pLeftFront = NULL;
CANTalon   *g_pRightFront = NULL;

DigitalInput *g_pShoulderLimit1   = NULL;
DigitalInput *g_pShoulderLimit2    = NULL;
DigitalInput *g_pElbowLimit1   = NULL;
DigitalInput *g_pElbowLimit2    = NULL;
DigitalInput *g_pBarrelLimit1    = NULL;
DigitalInput *g_pShieldLimit1    = NULL;
DigitalInput *g_pBallSensor    = NULL;
Arm 		 *g_pArm	= NULL;
CANTalon 	 *g_pFan 	= NULL;
CANTalon 	 *g_pBarrel = NULL;
CANTalon 	 *g_pShield = NULL;
CANTalon 	 *g_pShootWheels = NULL;
Servo		 *g_pFireServo = NULL;
ShooterMech  *g_pShoot	= NULL;

DoubleSolenoid *g_pRightBig = NULL;
DoubleSolenoid *g_pLeftBig = NULL;
DoubleSolenoid *g_pRightSmall = NULL;
DoubleSolenoid *g_pLeftSmall = NULL;

PistonMech *g_pPiston = NULL;

float 		  g_currentX = 0;
float 		  g_currentY = 0;

std::shared_ptr<NetworkTable> g_dataTable;
std::shared_ptr<NetworkTable> g_dTA;

std::mutex g_targetMutux;

Robot::Robot()
	 :   m_tiltServo(1), m_panServo(2), m_leftFront(13/*13*/), m_leftRear(11/*11*/), m_rightFront(14/*14*/), m_rightRear(12/*12*/),
		 /*m_drive(m_leftRear, m_rightRear),*/ m_driveLeftJoyStick(0), m_driveRightJoyStick(1), m_auxLeftJoyStick(2), m_auxRightJoyStick(3),
		 m_motor1(23), m_elbow(22),m_shoulder(21),m_pAHRS(NULL),// m_accel(I2C::kOnboard),
		 m_shoulderLimit1(4), m_shoulderLimit2(5), m_elbowLimit1(6), m_elbowLimit2(7), m_barrelLimit1(0), m_shieldLimit1(2), m_ballSensor(3), //m_arm(),
		 m_fan(31), m_barrel(32), m_shield(33), m_shootWheels(34), m_fireServo(0), m_shoot()
		 ,m_drive(m_leftFront, m_leftRear, m_rightFront, m_rightRear),
		 m_rightBig(0,1), m_leftBig(2,3), m_rightSmall(4,5), m_leftSmall(6,7),m_pistonMech()
{
	g_pLeftRear  =	&m_leftRear;
	g_pRightRear  =	&m_rightRear;
	g_pLeftFront  =	&m_leftFront;
	g_pRightFront  =&m_rightFront;
	g_pDrive      = &m_drive;
	g_pDRJS        = &m_driveRightJoyStick;
	g_pDLJS        = &m_driveLeftJoyStick;
	g_pALJS        = &m_auxLeftJoyStick;
	g_pARJS		   = &m_auxRightJoyStick;
	g_pPanTilt    = &m_panTilt;
	g_pmotor1     = &m_motor1;
	g_pShoulder     = &m_shoulder;
	g_pElbow     = &m_elbow;
	//g_pAccel	  = &m_accel;
	g_pShoulderLimit1 = &m_shoulderLimit1;
	g_pShoulderLimit2  = &m_shoulderLimit2;
	g_pBarrelLimit1  = &m_barrelLimit1;
	g_pShieldLimit1  = &m_shieldLimit1;
	g_pElbowLimit1 = &m_elbowLimit1;
	g_pElbowLimit2  = &m_elbowLimit2;
	g_pBallSensor  = &m_ballSensor;
	g_pArm		  = &m_arm;
	g_pFan			= &m_fan;
	g_pBarrel		= &m_barrel;
	g_pShield		= &m_shield;
	g_pShootWheels	= &m_shootWheels;
	g_pFireServo	= &m_fireServo;
	g_pShoot		= &m_shoot;
	g_pRightBig		= &m_rightBig;
	g_pLeftBig		= &m_leftBig;
	g_pRightSmall	= &m_rightSmall;
	g_pLeftSmall	= &m_leftSmall;
	g_pPiston	    = &m_pistonMech;

	m_panTilt.SetServos(&m_panServo, &m_tiltServo);

	g_pPanTilt = &m_panTilt;

	m_leftFront.SetSafetyEnabled(false);
	m_leftRear.SetSafetyEnabled(false);
	m_rightFront.SetSafetyEnabled(false);
	m_rightRear.SetSafetyEnabled(false);
	m_drive.SetSafetyEnabled(false);
	m_barrel.SetSafetyEnabled(false);
	m_shield.SetSafetyEnabled(false);
	m_fan.SetSafetyEnabled(false);
	m_shootWheels.SetSafetyEnabled(false);

	m_leftFront.SetInverted(true);
	m_leftRear.SetInverted(true);
	m_rightFront.SetInverted(true);
	m_rightRear.SetInverted(true);

	extern void SerialThread();

	m_serialThread = std::thread(SerialThread);
}

void Robot::RobotInit()
{
	Image *frame = imaqCreateImage(IMAQ_IMAGE_RGB, 0);
	imaqDispose(frame);
	InitGyro();
	InitPID();
	InitDataTable();
	//g_dataTable->PutNumber("P-s", 1.0);
	//g_dataTable->PutNumber("I-s", 0.0);
	//g_dataTable->PutNumber("D-s", 0.0);
	//g_dataTable->PutNumber("P-e", 1.0);
	//g_dataTable->PutNumber("I-e", 0.0);
	//g_dataTable->PutNumber("D-e", 0.0);
	InitShoot();
	//g_pArm->ArmInit();
}

void Robot::TestInit()
{
	//m_testMode.Init();
}

void Robot::TestPeriodic()
{
	//m_testMode.Periodic();
}
void Robot::AutonomousInit()
{
	m_autonomous.Init();
	m_shoot.Init();
}

void Robot::AutonomousPeriodic()
{
	m_autonomous.Periodic();
	m_shoot.Periodic();
}

void Robot::TeleopInit()
{
	InitShoot();

	/*double pe = g_dataTable->GetNumber("P-e", 1.2);
	double ie = g_dataTable->GetNumber("I-e", 0.001);
	double de = g_dataTable->GetNumber("D-e", 0.0);
	g_pElbow->SetPID(pe, ie, de);
	double ps = g_dataTable->GetNumber("P-s", 1.2);
	double is = g_dataTable->GetNumber("I-s", 0.0005);
	double ds = g_dataTable->GetNumber("D-s", 0.0);
	g_pShoulder->SetPID(ps, is, ds);*/
}

void Robot::TeleopPeriodic()
{
	//printf("\r\nWheel Speed: %.6f, Shield Limit: %d, Time: %.6f, Deflector Pos: %.6f", g_pShootWheels->Get(), g_pBarrelLimit1->Get(),GetTime(), g_pShield->GetPosition());

	/*
	double TargetSpeed = g_dataTable->GetNumber("Target Speed");

	g_pLeftRear->Set(TargetSpeed);
	g_pLeftFront->Set(TargetSpeed);
	g_pRightRear->Set(TargetSpeed);
	g_pRightFront->Set(TargetSpeed);
	*/


	/*
	double LJSSpeed  = g_pLJS->GetY() * JS_MULTIPLIER;

	double RJSSpeed = g_pRJS->GetY() * JS_MULTIPLIER;
	double TargetSpeedLeft = LJSSpeed;
	double TargetSpeedRight = RJSSpeed;


	//DEAD BAND
	if(LJSSpeed < -DRIVE_DEADBAND)
	{
		TargetSpeedLeft = LJSSpeed + DRIVE_DEADBAND;
	}
	else if(LJSSpeed > DRIVE_DEADBAND)
	{
		TargetSpeedLeft = LJSSpeed - DRIVE_DEADBAND;
	}
	else
	{
		TargetSpeedLeft = 0;
	}

	if(RJSSpeed < -DRIVE_DEADBAND)
	{
		TargetSpeedRight = RJSSpeed + DRIVE_DEADBAND;
	}
	else if(RJSSpeed > DRIVE_DEADBAND)
	{
		TargetSpeedRight = RJSSpeed - DRIVE_DEADBAND;
	}
	else
	{
		TargetSpeedRight = 0;
	}

	g_pLeftRear->Set(TargetSpeedLeft);
	g_pLeftFront->Set(TargetSpeedLeft);
	g_pRightRear->Set(TargetSpeedRight);
	g_pRightFront->Set(TargetSpeedRight);


	//printf("Teleop Periodic\n\r");
	/*int errorX, errorY;

	extern void GetTarget(int &errorX, int &errorY);

	GetTarget(errorX, errorY);

	if(errorX == -1000 || errorY == -1000)
	{
		iterator++;
		if(iterator>5)
		{
			g_pRightRear->Set(150);
			g_pLeftRear->Set(150);
		}
	}
	else
	{
		iterator = 0;
		g_pRightRear->Set(0);
		g_pLeftRear->Set(0);
	}*/

	//extern void TrackTarget();

	//TrackTarget();

	//g_pDrive->TankDrive(g_pDLJS->GetY(),g_pDRJS->GetY());
	//g_pDrive->ArcadeDrive(g_pLJS->GetY(),g_pLJS->GetZ());

	extern void TrackTarget();
	TrackTarget();

	UpdatePiston();
	//g_pArm->ArmPeriodic();
	UpdateShoot();

	m_driveLeftJoyStick.UpdateJoystick();
	m_driveRightJoyStick.UpdateJoystick();
	m_auxRightJoyStick.UpdateJoystick();
	m_auxLeftJoyStick.UpdateJoystick();
}

void Robot::DisabledInit()
{

}

void Robot::DisabledPeriodic()
{

}
START_ROBOT_CLASS(Robot)
