/*
 * AutomaticOpList.h
 *
 *  Created on: Feb 10, 2016
 *      Author: NYSC
 */

#ifndef AUTOMATICOPLIST_H_
#define AUTOMATICOPLIST_H_

#include <AutomaticOperation.h>
#include <vector>


typedef std::vector<AutomaticOperation *> AutomaticOps;

class AutomaticOpList
{
	public:
		void Add(AutomaticOperation *pOP)
		{
			m_ops.push_back(pOP);
		}

		//returns true when done, false otherwise
		bool Periodic()
		{
			if (m_ops.size() == 0)
				return true;

			AutomaticOps::iterator iter = m_ops.begin();

			if ((*iter)->Periodic())
			{
				delete (*iter);

				m_ops.erase(iter);
			}

			return false;
		}

		void Clear()
		{
			for(AutomaticOps::iterator iter = m_ops.begin(); iter!=m_ops.end();iter++)
			{
				delete (*iter);
			}
			m_ops.clear();
		}

	protected:
		AutomaticOps m_ops;


};


#endif /* AUTOMATICOPLIST_H_ */
