#include "Autonomous.h"
#include "WPILib.h"
#include "PanTilt.h"
#include "TestMode.h"
#include "navx_frc_cpp\src\AHRS.h"
#include "Arm.h"
#include "JS.h"
#include "ShooterMech.h"
#include "PistonMech.h"

#define PVAL 10
#define IVAL 0
#define DVAL 0

#define DRIVE_P 2.5
#define DRIVE_I 0.008 //0.0005
#define DRIVE_D 0.0

#define JS_MULTIPLIER 250
#define DRIVE_DEADBAND 0.1*JS_MULTIPLIER

class Robot: public IterativeRobot
{
	public    :
				Robot();
	protected :
				void InitPID();
				void InitGyro();
				void ArmInit();
				void InitJoshDataTable();
				void InitDataTable();
				void UpdatePID();
				void UpdateArm();
				void UpdateJoshDataTable();
				void UpdateDataTable();
				void UpdateShoot();
				void InitShoot();
				void UpdatePiston();

				double LinearDeadband(double speed, double deadband);
				double SlopedDeadband(double speed, double deadband);
	private   :
				void RobotInit();
				void AutonomousInit();
				void AutonomousPeriodic();
				void TeleopInit();
				void TeleopPeriodic();
				void TestInit();
				void TestPeriodic();
				void DisabledInit();
				void DisabledPeriodic();


				Servo          m_tiltServo, m_panServo;
				CANTalon       m_leftFront, m_leftRear, m_rightFront, m_rightRear;

				JS		 	   m_driveLeftJoyStick;
				JS		 	   m_driveRightJoyStick;
				JS			   m_auxLeftJoyStick;
				JS             m_auxRightJoyStick;

				CANTalon	   m_motor1,m_elbow, m_shoulder;
				AHRS          *m_pAHRS;
				DigitalInput   m_shoulderLimit1;
				DigitalInput   m_shoulderLimit2;
				DigitalInput   m_elbowLimit1;
				DigitalInput   m_elbowLimit2;
				DigitalInput   m_barrelLimit1;
				DigitalInput   m_shieldLimit1;
				DigitalInput   m_ballSensor;
				Arm		       m_arm;
				CANTalon	   m_fan, m_barrel, m_shield, m_shootWheels;
				Servo		   m_fireServo;
				std::thread    m_serialThread;

				CPanTilt       m_panTilt;
				CAutonomous    m_autonomous;
				CTestMode      m_testMode;
				ShooterMech    m_shoot;
				RobotDrive     m_drive;

				DoubleSolenoid m_rightBig;
				DoubleSolenoid m_leftBig;
				DoubleSolenoid m_rightSmall;
				DoubleSolenoid m_leftSmall;

				PistonMech     m_pistonMech;

				bool           m_bbigPistonOut = false;
};
