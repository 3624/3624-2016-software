/*
 * Joystick.cpp
 *
 *  Created on: Feb 13, 2016
 *      Author: Michael ws
 */

#include "JS.h"

JS::JS(int i)
   :Joystick(i)
{
	for(int x=0; x<12; x++)
	{
		m_bButtonUp[x]   = false;
		m_bButtonDown[x] = false;
		m_bButtons[x]    = false;
	}
}

void JS::UpdateJoystick()// call at end of periodic
{
	for(int i= 0; i<12; i++)
	{
		m_bButtonUp[i]   = false;
		m_bButtonDown[i] = false;

		if (GetRawButton(i + 1) != m_bButtons[i])
		{
			m_bButtons[i] = !m_bButtons[i];

			if (m_bButtons[i])
			{
				m_bButtonDown[i] = true;
			}
			else
			{
				m_bButtonUp[i] = true;
			}

			m_buttonCounter[i] = 0;
		}
	}
}

bool JS::IsHeld(int i)
{
	return GetRawButton(i);
}

bool JS::IsPressed(int i)
{
	return m_bButtonDown[i-1];
}

bool JS::IsReleased(int i)
{
	return m_bButtonUp[i-1];
}


