/*
 * PID.cpp
 *
 *  Created on: Feb 5, 2016
 *      Author: NYSC
 */
#include "Robot.h"
#include <PID.h>

extern CANTalon   *g_pLeftRear;
extern CANTalon   *g_pRightRear;
extern CANTalon    *g_pElbow;
extern CANTalon   *g_pShoulder;
extern CANTalon	  *g_pBarrel;
extern CANTalon	  *g_pShield;
extern std::shared_ptr<NetworkTable> g_dataTable;
extern CANTalon   *g_pShootWheels;

void Robot::InitPID()
{

	g_pBarrel->SetControlMode(CANSpeedController::kPosition);
	g_pShield->SetControlMode(CANSpeedController::kPosition);

	g_pBarrel->SetFeedbackDevice(CANTalon::QuadEncoder);
	g_pShield->SetFeedbackDevice(CANTalon::QuadEncoder);

	g_pShield->ConfigEncoderCodesPerRev(360);
	g_pBarrel->ConfigEncoderCodesPerRev(360);


	g_pShootWheels->SetControlMode(CANSpeedController::kSpeed);
	g_pShootWheels->SetFeedbackDevice(CANTalon::QuadEncoder);
	g_pShootWheels->ConfigEncoderCodesPerRev(360);
	g_pShootWheels->SetClosedLoopOutputDirection(true);
	g_pShootWheels->SetPID(.8, .001, 0);


	g_pBarrel->SetPID(5, .0005, 0);
	g_pShield->SetPID(1, 0, 0);

}

void Robot::UpdatePID()
{
	/*double val = g_dataTable->GetNumber("Value", 0.0);

	double P = g_dataTable->GetNumber("P", 1.0);
	double I = g_dataTable->GetNumber("I", 0.0);
	double D = g_dataTable->GetNumber("D", 0.0);

	g_pLeftRear->SetPID(P, I, D);
	g_pRightRear->SetPID(P, I, D);

	g_pLeftRear->Set(val);
	g_pRightRear->Set(-val);

	//g_dataTable->PutNumber("Left Encoder", g_pLeftRear->GetPosition());
	//g_dataTable->PutNumber("Right Encoder", g_pRightRear->GetPosition());
	g_dataTable->PutNumber("Left Error", g_pLeftRear->GetClosedLoopError());
	g_dataTable->PutNumber("Right Error", g_pRightRear->GetClosedLoopError());

	//g_dataTable->PutNumber("Accel X", g_pAccel->GetX());
	//g_dataTable->PutNumber("Accel Y", g_pAccel->GetY());
	//g_dataTable->PutNumber("Accel Z", g_pAccel->GetZ());

	printf("\"%d\", \"%d\"\n\r", g_pLeftRear->GetClosedLoopError(), g_pRightRear->GetClosedLoopError());

	//g_dataTable->PutNumber("Left Error", errorLeft);
	//g_dataTable->PutNumber("Right Error", errorRight);

	//g_pLeftRear->Set(0);
	//g_pRightRear->Set(0);*/
}

