#include "Teleop.h"
#include "Autonomous.h"
#include "WPILib.h"
#include "PanTilt.h"

class Robot: public IterativeRobot
{
	public  :
				Robot();
	private :
				void RobotInit();
				void AutonomousInit();
				void AutonomousPeriodic();
				void TeleopInit();
				void TeleopPeriodic();
				//void TestInit();
				//void TestPeriodic();
				//void DisabledInit();
				//void DisabledPeriodic();

				Servo          m_tiltServo, m_panServo;
				CANTalon       m_leftFront, m_leftRear, m_rightFront, m_rightRear;
				RobotDrive     m_drive;
				Joystick 	   m_leftJoyStick;
				Joystick 	   m_rightJoyStick;

				CPanTilt       m_panTilt;
				CTeleOp        m_teleOp;
				CAutonomous    m_autonomous;

};
