#include "Teleop.h"
#include "PanTilt.h"

extern RobotDrive *g_pDrive;
extern Joystick   *g_pLJS;
extern Joystick   *g_pRJS;
extern CPanTilt   *g_pPanTilt;

extern std::shared_ptr<NetworkTable> g_dataTable;

CTeleOp::CTeleOp()
{
}

CTeleOp::~CTeleOp()
{

}

void CTeleOp::Init()
{
	CRunMode::Init();

	g_pPanTilt->SetPanTilt(PAN_0_DEGREES, TILT_0_DEGREES);
}

void CTeleOp::Periodic()
{
	CRunMode::Periodic();

	double camXError = g_dataTable->GetNumber("CamXError", 0.0);
	double camYError = g_dataTable->GetNumber("CamYError", 0.0);

	if (camXError == -1000 || camYError == -1000)
	{

	}
	else
	{
		double newPan  = g_pPanTilt->GetPan()  + .00003f * camXError;
		double newTilt = g_pPanTilt->GetTilt() + .00003f * camYError;

		g_pPanTilt->SetPanTilt(newPan, newTilt);
	}

	g_pDrive->TankDrive(g_pLJS->GetY(), g_pRJS->GetY()); // drive with tank style
}
