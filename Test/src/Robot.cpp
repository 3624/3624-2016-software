#include "WPILib.h"
#include "Robot.h"


RobotDrive *g_pDrive   = NULL;
Joystick   *g_pRJS     = NULL;
Joystick   *g_pLJS     = NULL;
CPanTilt   *g_pPanTilt = NULL;

std::shared_ptr<NetworkTable> g_dataTable;

Robot::Robot()
	 :   m_tiltServo(0), m_panServo(1), m_leftFront(13), m_leftRear(11), m_rightFront(14), m_rightRear(12),
		 m_drive(m_leftFront, m_leftRear, m_rightFront, m_rightRear), m_leftJoyStick(0), m_rightJoyStick(1)
{
	g_pDrive   = &m_drive;
	g_pRJS     = &m_rightJoyStick;
	g_pLJS     = &m_leftJoyStick;
	g_pPanTilt = &m_panTilt;

	m_panTilt.SetServos(&m_panServo, &m_tiltServo);

	g_pPanTilt = &m_panTilt;

	m_leftFront.SetSafetyEnabled(false);
	m_leftRear.SetSafetyEnabled(false);
	m_rightFront.SetSafetyEnabled(false);
	m_rightRear.SetSafetyEnabled(false);
	m_drive.SetSafetyEnabled(false);

	m_leftFront.SetInverted(true);
	m_leftRear.SetInverted(true);
	m_rightFront.SetInverted(true);
	m_rightRear.SetInverted(true);
}

void Robot::RobotInit()
{
	Image *frame = imaqCreateImage(IMAQ_IMAGE_RGB, 0);


	g_dataTable = NetworkTable::GetTable("datatable");

	imaqDispose(frame);
}

void Robot::AutonomousInit()
{
	m_autonomous.Init();
}

void Robot::AutonomousPeriodic()
{
	m_autonomous.Periodic();
}

void Robot::TeleopInit()
{
	m_teleOp.Init();
}

void Robot::TeleopPeriodic()
{
	m_teleOp.Periodic();
}

START_ROBOT_CLASS(Robot)
