#include "WPILib.h"
#include "RunMode.h"

class CTeleOp : public CRunMode
{
	public    : CTeleOp();
				virtual ~CTeleOp();

				void Init();
				void Periodic();
};
