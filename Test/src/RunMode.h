#ifndef SRC_RUNMODE_H_
#define SRC_RUNMODE_H_

#include <WPILib.h>

class CRunMode
{
	public    : CRunMode();
				virtual ~CRunMode();

				virtual void Init();
				virtual void Periodic();
	protected :
				double m_initTime, m_deltaTime;
				int m_gyroOffset;
};


#endif /* SRC_RUNMODE_H_ */
