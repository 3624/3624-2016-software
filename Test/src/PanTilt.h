#ifndef SRC_PANTILT_H_
#define SRC_PANTILT_H_

#include <WPILib.h>

#define PAN_0_DEGREES  0.51414
#define TILT_0_DEGREES 0.61188

class CPanTilt
{
	public    : CPanTilt();

				void SetServos(Servo *pPan, Servo *pTilt);
				void SetPanTilt(double pan, double tilt);
				void SetPan(double pan);
				void SetTilt(double tilt);
				double GetPan();
				double GetTilt();

	protected :
				Servo *m_pPan, *m_pTilt;
				double m_pan, m_tilt;
};



#endif
