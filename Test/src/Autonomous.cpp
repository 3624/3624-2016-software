#include "Autonomous.h"

extern RobotDrive *g_pDrive;
extern Joystick   *g_pRJS, *g_pLJS;

CAutonomous::CAutonomous()
{
}


CAutonomous::~CAutonomous()
{

}

void CAutonomous::Init()
{
	CRunMode::Init();
}

void CAutonomous::Periodic()
{
	CRunMode::Periodic();
}
