#include "WPILib.h"
#include "RunMode.h"

class CAutonomous : public CRunMode
{
	public    : CAutonomous();
				virtual ~CAutonomous();

				void Init();
				void Periodic();
};
