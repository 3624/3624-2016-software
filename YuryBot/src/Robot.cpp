#include "WPILib.h"

/**
 * This is a demo program showing the use of the RobotDrive class.
 * The SampleRobot class is the base of a robot application that will automatically call your
 * Autonomous and OperatorControl methods at the right time as controlled by the switches on
 * the driver station or the field controls.
 *
 * WARNING: While it may look like a good choice to use for your code if you're inexperienced,
 * don't. Unless you know what you are doing, complex code will be much more difficult under
 * this system. Use IterativeRobot or Command-Based instead if you're new.
 */
class Robot: public SampleRobot
{
	CANTalon leftFront, leftRear, rightFront, rightRear;
	RobotDrive myRobot; // robot drive system
	Joystick stick; // only joystick

public:
	Robot() :
		    leftFront(13), leftRear(14), rightFront(11), rightRear(12),
			myRobot(leftFront, leftRear, rightFront, rightRear),	// initialize the RobotDrive to use motor controllers on ports 0 and 1
			stick(0)
	{
		leftFront.SetSafetyEnabled(false);
		leftRear.SetSafetyEnabled(false);
		rightFront.SetSafetyEnabled(false);
		rightRear.SetSafetyEnabled(false);
		myRobot.SetSafetyEnabled(false);

		myRobot.SetExpiration(100);
	}

	void RobotInit()
	{
		Image *frame = imaqCreateImage(IMAQ_IMAGE_RGB, 0);

		imaqDispose(frame);
	}

	/**
	 * Runs the motors with arcade steering.
	 */
	void OperatorControl()
	{
		while (IsOperatorControl() && IsEnabled())
		{//Stop beating me...
			printf("%.3f, %.3f\r\n", stick.GetX(), stick.GetY());
			myRobot.ArcadeDrive(stick); // drive with arcade style (use right stick)
			Wait(0.005);				// wait for a motor update time
		}
	}

};

START_ROBOT_CLASS(Robot)
